# Chart Evaluator

Evaluates your performance between bar charts and pie charts.

## Requirements

- Python 3
- matplotlib (you can install through the console using `pip3 install matplotlib`)

## Instructions

- Step 1. Ignore any warnings during runtime, as long as the program says "Finish" then you're good.
- Step 2. Run program and follow its instructions using `python3 main.py`
- Step 3. Data is stored in the file named `results.csv`
