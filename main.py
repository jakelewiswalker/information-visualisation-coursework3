"""
Group:
Jake Walker
Jake Wong
Zecheng Hu
Gordon Zhou
"""

# import various libraries
import sys
import random
import time
import numpy as np
import matplotlib.pyplot as plt

"""
Handles the main evaluation phase.
Showing different graph types to the user,
recording their performance,
and outputing the results to a file.
"""
class Evaluator:
    """
    Constructor:
    -initialises some variables
    -registers the press callback function (you must register the callback before plt.show()
    Inputs: fig: the plotting figure that is used to register callbacks
    """
    def __init__(self, fig):
        # seed using current time
        random.seed()

        self.chart_number = 0 # counts from 1 to 20
        self.highest_index = 0 # 0 to 4, index of the grade with the highest percentage
        self.paused = True # Ignores press events when True
        self.ask_time = time.time() # last time the chart is drawn
        self.showing_bar_chart = bool(random.getrandbits(1)) # the last shown chart type, initially a random choice between True and False
        self.bar_chart_results = [] # array of tuples (response time, correct/wrong)
        self.pie_chart_results = [] # array of tuples (response time, correct/wrong)
        self.is_finished = False # True only when final results are recorded

        # graph showing the percentage of students achieving different grades
        self.grades = ['1st\n(press 1)', '2:1\n(press 2)', '2:2\n(press 3)', '3rd\n(press 4)', 'Fail\n(press 5)']
        # percentage values of each grade, 0 to 100
        self.percentages = [0, 0, 0, 0, 0]
        # x position for each bar on the bar chart
        self.x_pos = [i for i, _ in enumerate(self.grades)]
        # register the callback function
        fig.canvas.mpl_connect('key_press_event', self.press)

    """
    Function: Starts the evaluation phase. Shows multiple charts sequentially.
    """
    def start(self):
        # let the user have a little time to prepare
        plt.pause(1)
        self.paused = False
        self.draw_chart()

    """
    Inputs: event: this is the event key pressed.
    Function: Keypress event callback function, used to record the users response to each chart.
    Records the response time, correctness and shows the next chart.
    """
    def press(self, event):
        if self.is_finished:
            # results are recorded and the user has read the finished message
            # close down plotting window
            plt.close()
            return

        if self.paused:
            # this is during the blank screen
            return

        sys.stdout.flush()
        if event.key in ['1', '2', '3', '4', '5']:
            # record response time
            response_time = time.time() - self.ask_time
            # record answer correctness
            answer_correctness = int(event.key) == self.highest_index + 1

            if self.showing_bar_chart:
                self.bar_chart_results.append((response_time, answer_correctness))
            else:
                self.pie_chart_results.append((response_time, answer_correctness))

            if 20 <= self.chart_number:
                # we've shown all the charts, finish up
                # write results to a csv file
                file = open("results.csv", "w")
                file.write("bar response time, bar answer correct, pie response time, pie answer correct\n")
                for i in range(len(self.bar_chart_results)):
                    file.write('{}, {}, {}, {}\n'.format(self.bar_chart_results[i][0], self.bar_chart_results[i][1], self.pie_chart_results[i][0], self.pie_chart_results[i][1]))
                file.close()

                # tell the user we are finished
                plt.clf()
                plot_text("Finish! Press space or x to exit.", 10)
                # close the window next time the user presses a key
                self.is_finished = True

            else:
                # clear the current figure
                plt.clf()
                self.paused = True

                # pause for 1 second, AND ALSO UPDATES THE FIGURE
                plt.pause(1)
                self.paused = False
                self.draw_chart()

    """
    Function: draws the next chart, it will count how many charts it has drawn,
    and at chart number 11, it will start drawing the other chart type.
    """
    def draw_chart(self):
        # record the time
        self.ask_time = time.time()

        # randomise graph percentages
        self.highest_index = random.randint(0,len(self.percentages)-1)
        total = 0.0
        for i in range(len(self.percentages)):
            if i == self.highest_index:
                # choose random value from upper 10 percentile
                self.percentages[i] = random.uniform(0.9, 1.0)
            else:
                # choose random value from lower 60 percentile
                self.percentages[i] = random.uniform(0.2, 0.8)
            total += self.percentages[i]

        # make sure percentages add up to 100%
        for i in range(len(self.percentages)):
            self.percentages[i] = 100 * self.percentages[i] / total

        self.chart_number += 1
        if self.chart_number == 11:
            self.showing_bar_chart = not self.showing_bar_chart

        if self.showing_bar_chart:
            # show a bar chart next
            plt.bar(self.x_pos, self.percentages) # color='g'
            plt.xlabel("Degree Classification")
            plt.ylabel("Percentage")
            plt.xticks(self.x_pos, self.grades)
            plt.title("Distribution of degree classifications (UK - May 2020)")
        else:
            # show a pie chart next
            plt.pie(self.percentages, labels=self.grades)
            plt.title("Distribution of degree classifications (UK - May 2020)")

        # draws the figure, doesn't block
        plt.draw()

#PREDEFINED VARIABLE DECLARATION
fig, ax = plt.subplots()
textshow = [["Press space bar to continue these slides when you are ready. Or press x to exit.",8],
            ["You will be shown different charts between a 1 second delay.",10],
            ["Press 1, 2, 3, 4, 5 to pick the largest category of degree classification.",9]] #text show is a line of text per graph.
countertext = 0 #counter of current index of text we are showing
connection_id = 0 #id of press callback event
evaluator = Evaluator(fig)

"""
    Inputs: event: this is the event key pressed.
    Function: Keypress event callback function, which is used in the first section of the experiment to register when
    the user is stepping through to the next slide.
"""
def press(event):
    global countertext, connection_id, fig

    sys.stdout.flush()
    if event.key == ' ':
        if countertext < len(textshow):
            plt.clf()
            plt.pause(1)
            plot_text(textshow[countertext][0], textshow[countertext][1])
            countertext+=1
        else:
            fig.canvas.mpl_disconnect(connection_id) # disconnect this press event
            plt.clf() # clear our current slide
            evaluator.start() # start the evaluator

    if event.key == 'x':
        exit()

#Links the keypress event to the code in order to function
connection_id = fig.canvas.mpl_connect('key_press_event', press)

"""
    Inputs: text: the text to plot on the graph
            size: the size of the text we are plotting on the graph
    Function: This function plots some text onto the open window
"""
def plot_text(text, size):
    plt.clf()
    xs = np.arange(0,0,1)
    ys = np.arange(0,0,1)
    plt.plot(xs,ys)
    plt.text(0,0,text,fontsize=size,horizontalalignment='center')
    plt.draw()

"""
    Function: function algorithm that initiates the code in order to run
"""
def main():
    #First code section to run the count down and instructions
    global countertext
    plot_text(textshow[countertext][0], textshow[countertext][1])
    countertext+=1
    plt.show()

main()
